Feature: Validate the new Mulesoft Api

  Scenario Outline: validate the mulesoft api reponses against the db and salesforce

    Given I hit the "<muleApi>"
    And I hit the "<prodApi>"
    Then I caompare response with "<muleApi>" and "<prodApi>" responses
    Then I validate against the "<target>"


    @mule-utils
    Examples:
    |muleApi | target | prodApi |
    | UtilityToken | salesforce | IdToken |