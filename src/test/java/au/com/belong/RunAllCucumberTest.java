package au.com.belong;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true, format = { "pretty", "html:target/cucumber", "json:target/cucumber/cucumber.json",
		"rerun:target/cucumber/rerun.txt" }, features = "src/test/resources/feature", glue = {
				"au.com.belong.stepdefinitions" }, tags = { "@mule-utils" })
public class RunAllCucumberTest {
}