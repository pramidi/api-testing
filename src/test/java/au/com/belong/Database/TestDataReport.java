package au.com.belong.Database;

import au.com.belong.util.DatabaseConnection;
import au.com.belong.util.MongoDriver;
import com.mongodb.client.FindIterable;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;


public class TestDataReport {

	private static final Logger LOGGER = LoggerFactory.getLogger(TestDataReport.class);


	public ResultSet getAddressDataFromPreprod(String query) throws SQLException {
		String username = "admin";
		String password = "admin123";
		DatabaseConnection databaseConnection = new DatabaseConnection();
		Connection preprod = databaseConnection.connectEveDB("jdbc:mysql://preprod-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		return databaseConnection.processSQLStatement(query, preprod);

	}

	public static void rollBackEnvironmentName() throws SQLException {

		DatabaseConnection databaseConnection_Preprod = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa1 = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa2 = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa3 = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa4 = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa5 = new DatabaseConnection();
		DatabaseConnection databaseConnection_qa6 = new DatabaseConnection();
		String username = "admin";
		String password = "admin123";
		int count=0;

		Connection preprod = databaseConnection_Preprod.connectEveDB("jdbc:mysql://preprod-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c2 = databaseConnection_qa1.connectEveDB("jdbc:mysql://qa1-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c3 = databaseConnection_qa2.connectEveDB("jdbc:mysql://qa2-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c4 = databaseConnection_qa3.connectEveDB("jdbc:mysql://qa3-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c5 = databaseConnection_qa4.connectEveDB("jdbc:mysql://qa4-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c6 = databaseConnection_qa5.connectEveDB("jdbc:mysql://qa5-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);
		Connection c7 = databaseConnection_qa6.connectEveDB("jdbc:mysql://qa6-rds.cecmg2xsladv.ap-southeast-2.rds.amazonaws.com:3306/EVE", username, password);

		FindIterable<Document> docs = MongoDriver.getAlldocs("resultSet");
		for (Document d: docs
				) {

			try {

				String u = d.getString("utilBillID");
//				databaseConnection_Preprod.setConnection(c1);
//				databaseConnection_qa1.setConnection(c2);
//				databaseConnection_qa2.setConnection(c3);
//				databaseConnection_qa3.setConnection(c4);
//				databaseConnection_qa4.setConnection(c5);
//				databaseConnection_qa5.setConnection(c6);
				databaseConnection_qa6.setConnection(c7);
				ResultSet r = databaseConnection_Preprod.processSQLStatement("select UTILIBILL_ID from EVE.CUSTOMERS where UTILIBILL_ID = '" + u + "'");
				while (r.next()) {
					count++;
					String util = r.getString("UTILIBILL_ID");
					if (util.length() > 1) {
						Document ds = new Document();
						ds.put("environment", "qa6");
						ds.put("utilBillID", util);
						MongoDriver.updateDocument("resultSet", "utilBillID", ds);
//						LOGGER.info("updated "+util+ " with environment qa1");
//						LOGGER.info("updated "+util+ " with environment qa2");
//						LOGGER.info("updated "+util+ " with environment qa3");
//						LOGGER.info("updated "+util+ " with environment qa4");
//						LOGGER.info("updated "+util+ " with environment qa5");
						LOGGER.info("updated "+util+ " with environment qa6");
					}
				}
			} catch (Exception e) {
				LOGGER.info("failed");
			}
		}
		LOGGER.info("total updated -> "+count);


	}

	
}
