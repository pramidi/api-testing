package au.com.belong.Database;

import au.com.belong.Base;
import au.com.belong.common.SQLQueryContainer;
import au.com.belong.util.DatabaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.HashMap;
//import javax.swing.text.Document;

public class VerifyPendingOrderTable extends Base {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerifyPendingOrderTable.class);



    public static String getRandomAddressFromEve(HashMap<String, String> searchAddress) throws SQLException {
        LOGGER.info("Getting Randome address table.....");
        SQLQueryContainer.setRANDOM_ADDRESS_TABLE_QUERY(searchAddress);
//        ResultSet rs = executeDBQuery(SQLQueryContainer.getRANDOM_ADDRESS_TABLE_QUERY());
        ResultSet rs = executeDBQueryPreprod(SQLQueryContainer.getRANDOM_ADDRESS_TABLE_QUERY());
        String columnName = rs.getMetaData().getColumnName(1);
        String addrID = rs.getMetaData().getColumnName(2);
        String inPlace = rs.getMetaData().getColumnName(3);
        while (rs.next()) {
            Base.sessionvar.put(addrID, rs.getObject(addrID).toString().replace("\"", ""));
            try {
                Base.sessionvar.put(inPlace, rs.getObject(inPlace).toString().replace("\"", ""));
            } catch (Exception e) {
                LOGGER.info("setting INPLACE as NULL");
                Base.sessionvar.put(inPlace, "NULL");
            }
            return rs.getObject(columnName).toString().replace("\"", "");
        }
        return "";

    }
    public static ResultSet executeDBQueryPreprod(String qryStr) throws SQLException {
        TestDataReport testDataReport = new TestDataReport();
        return testDataReport.getAddressDataFromPreprod(qryStr);
    }

    public static String getRandomADSLAddressFromEve(HashMap<String, String> searchAddress) throws SQLException {
        LOGGER.info("Getting Random ADSL address from EVE.....");
        SQLQueryContainer.setRANDOM_ADSL_ADDRESS_QUERY(searchAddress);
//        ResultSet rs1 = executeDBQuery(SQLQueryContainer.getRANDOM_ADSL_ADDRESS_TABLE_QUERY());
        ResultSet rs = executeAddressQuery(SQLQueryContainer.getRANDOM_ADSL_ADDRESS_TABLE_QUERY());

        try {
            while (rs.next()) {
                Base.sessionvar.put("SERVICE_ADDRESS_ID", rs.getObject("SERVICE_ADDRESS_ID").toString().replace("\"", ""));
                try {
                    Base.sessionvar.put("INPLACE_TYPE", rs.getObject("INPLACE_TYPE").toString().replace("\"", ""));
                } catch (Exception e) {
                    LOGGER.info("setting INPLACE as NULL");
                    Base.sessionvar.put("INPLACE_TYPE", "NULL");
                }
                return rs.getObject("ADDRESS").toString().replace("\"", "");
            }
        }catch (Exception e){
            LOGGER.info("address is not found. returning empty....");
        }
        return "";

    }

    public static ResultSet executeAddressQuery(String qryStr){
        ResultSet rs = null;
        LOGGER.info("Running Query "+ qryStr);
        try
        {
            Connection conn = DriverManager.getConnection(System.getProperty("address.jdbc.databaseurl"),
                    System.getProperty("address.jdbc.username"),
                    System.getProperty("address.jdbc.password"));

            PreparedStatement stmt = conn.prepareStatement(qryStr);

            rs = stmt.executeQuery();
            // STEP 5: Extract data from result set
            return rs;
        }
        catch (SQLException e)
        {
            LOGGER.error("SQLException from EVE DB" + e);
        }
        return rs;
    }

    public static void updateDBQuery(String qryStr) {
        DatabaseConnection dbConnection = new DatabaseConnection();
        dbConnection.executeUpdate(qryStr);

    }

    
    public String getUtilBillID(String customerEmail) throws SQLException {

        ResultSet rs = executeDBQuery(SQLQueryContainer.getPENDING_ORDER_TABLE_QRY(customerEmail));
        String utilBillID= "";
        while(rs.next()){

            try {
                utilBillID = rs.getObject("UTILITY_BILL_ID").toString();
                break;
            }catch (Exception e){
                LOGGER.error("NO Entry in PENDING ORDER Table!!");
            }
        }
        return utilBillID;
    }

    public static ResultSet executeDBQuery(String qryStr) {
        DatabaseConnection dbConnection = new DatabaseConnection();
        ResultSet rs = dbConnection.processSQLStatement(qryStr);

        return rs;
    }



    public static void whiteListAddress() {
        updateDBQuery(SQLQueryContainer.updateAddresstoWhitelist(Base.sessionvar.getString("SERVICE_ADDRESS_ID"))); // add to whitelist table
        LOGGER.info("whitelisting of "+Base.sessionvar.getString("SERVICE_ADDRESS_ID")+" is success");
    }
}


/*
My SQL queries

get details from pending order table
Select SERVICE_CLASS, INPLACE_TYPE, BROADBAND_TECH_TYPE,
concat(SERVICE_STREET_NUMBER, ' ', SERVICE_STREET, ' ',SERVICE_STREET_TYPE,' ', SERVICE_SUBURB) as address
 from EVE.PENDING_ORDERS order by PENDING_ID desc;
 */