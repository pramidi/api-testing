package au.com.belong;

import au.com.belong.api.requestBodyAttributes.UtilityApiRequest;
import au.com.belong.util.MongoDriver;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.bson.Document;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import au.com.belong.util.DatabaseConnection;
import au.com.belong.util.SalesforceConnection;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Base {
	private static final int TIMEOUT_GENERAL=10000;
	public static boolean testStatus= true;
	public static String message = "";
	private static final Logger LOGGER = LoggerFactory.getLogger(Base.class);
	protected static ResultSet rs;
	private static int WAITTIME = 45;
	public static int TIME_OUT = 20;
	public static final String ID = "id";
	public static final String XPATH = "xpath";
	public static final String LINKTEXT = "linkText";
	public static final String NAME = "name";
	public static final String CSS = "cssSelector";
	public static final String CLASSNAME = "className";
	public static final String TAGNAME= "tagName";
	public static final String PARTIALLINKTEXT = "partialLinkText";

	public static UtilityApiRequest utilityApi = new UtilityApiRequest();

	public static Document sessionvar = new Document();
	public static Document refData;
	private Configuration configuration;

	@Before
	public void init()
	{
		configuration = new Configuration();
		configuration.setUpTestingEnvironmentVariables();

		try {
			DatabaseConnection dbConnection = new DatabaseConnection();
			dbConnection.connectEveDB();
			SalesforceConnection.connectSalesforceWS();
			MongoDriver.connectMongo();

			LOGGER.info("EVE DB CONNECTED");
			LOGGER.info("SALESFORCE CONNECTED");
		}catch (Exception e) {
			LOGGER.info("Error connecting DBs \n");
			e.printStackTrace();
		}

	}




	/*
	 * Method to return timestamp
	 * 
	 * We will use this for running the automated tests in Preprod or while WFH
	 */
	public static String timeStamp(){
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		 Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		 String timevar=sdf.format(timestamp);
		 timevar=timevar.replace(".", "");
		 return timevar;
	}

	/*
	 * Method to establish SSH connection
	 * 
	 * We will use this for running the automated tests in Preprod or while WFH
	 */

	public static void establishSSH() {
		String user = Configuration.getEnvironmentProperties().getProperty("ssh.username");
		String password = Configuration.getEnvironmentProperties().getProperty("ssh.password");
		String host = Configuration.getEnvironmentProperties().getProperty("ssh.host");
		int port = Integer.parseInt(Configuration.getEnvironmentProperties().getProperty("ssh.port"));
		int localport = Integer.parseInt(Configuration.getEnvironmentProperties().getProperty("ssh.localport"));
		int dbport = Integer.parseInt(Configuration.getEnvironmentProperties().getProperty("ssh.dbport"));

		try {
			JSch jsch = new JSch();
			Session session = jsch.getSession(user, host, port);
			String rhost = "localhost";
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no");
			LOGGER.info("Establishing SSL Connection...");
			session.connect();
			session.setPortForwardingL(localport, rhost, dbport);
		} catch (Exception e) {
			// System.err.print(e);
		}
	}


    public void loadKeyToBson(String key, Document fromDoc, Document toDoc) {
	    try{
	    	if(!(fromDoc.get(key)==null)) {
				toDoc.put(key, fromDoc.get(key).toString());
				LOGGER.info("Loaded " + key + "value as " + fromDoc.get(key).toString());
			}
        }catch (Exception e){
	        LOGGER.info("unable to load key "+ e);
        }


    }

//    public Object jsonToJava(JSONObject jsonObject){
//		return JSONObject.wrap(jsonObject);
//	}
//
    public String javaToJSON(Object object) throws IOException {
		return new ObjectMapper().writeValueAsString(object);
	}

	public <T> T jsontoJava(String string, Class<T> classs) throws IOException {
		return new ObjectMapper().readValue(string, classs);
	}



}
