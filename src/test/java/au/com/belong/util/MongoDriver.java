package au.com.belong.util;


import au.com.belong.Base;
import au.com.belong.Configuration;
import au.com.belong.Database.VerifyPendingOrderTable;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.ScriptException;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.mongodb.client.model.Projections.*;

public class MongoDriver {

	private static final Logger LOGGER = LoggerFactory.getLogger(MongoDriver.class);
	
    // To connect to mongodb server
    public static MongoClient mongoClient;
    public static MongoDatabase database;
    public static MongoCollection<Document> collection;
    public static BasicDBObject oCollection;
    public static BasicDBObject searchQuery = new BasicDBObject();
    public static Document docs;
    public static FindIterable<Document> doc;
    public static UpdateOptions updateOptions = new UpdateOptions();

    public static void connectMongo(){
        mongoClient = new MongoClient( "10.0.3.238" , 27017);
    }

    public static void ConnectToMongo(String dbName) {

      try{
//          mongoClient = new MongoClient( "local" , 27017); //user this incase of cloud mongo is down
         database = mongoClient.getDatabase(dbName);
//         boolean auth = db.authenticate(myUserName, myPassword);
//         boolean auth = database.authenticate(myUserName, myPassword);
//         System.out.println("Authentication: "+auth);
         
      }catch(Exception e){
         LOGGER.error( e.getClass().getName() + ": " + e.getMessage() );
         LOGGER.error("Failed to Connect database");
      }
   }

   public static void loadrefData(String scenario, String table) throws Exception {
       Base.refData=MongoDriver.getData(scenario,table);
       String targetDB="";
       String address ="";
       if(Base.refData.containsKey("searchCriteria")) {
           Document searchQuery= (Document) Base.refData.get("searchCriteria");
           if (searchQuery.containsKey("DATABASE")){
               targetDB = searchQuery.getString("DATABASE");
               searchQuery.remove("DATABASE");
           }
           if (targetDB.equalsIgnoreCase("MONGO")){
        	   Base.refData.put("address",MongoDriver.searchRandomDataFromMongo("PendingOrderPreProd",searchQuery).getString("ADDRESS").replace("\"",""));
               LOGGER.info("Getting data from MONGO");
               Base.sessionvar.put("Address", Base.refData.getString("address"));
	        }
	        else
	        {
	            HashMap<String, String> searchAddress = new HashMap<String, String>();
	            searchAddress.putAll((Map<? extends String, ? extends String>) Base.refData.get("searchCriteria"));
	            if(scenario.contains("ADSL")) {
	                LOGGER.info("getting random ADSL address from EVE");
                    address = VerifyPendingOrderTable.getRandomADSLAddressFromEve(searchAddress);
	            }else {
                    LOGGER.info("getting random NBN address from EVE");
                    address = VerifyPendingOrderTable.getRandomAddressFromEve(searchAddress);
	            }
	            if(!(address==null)) {
                    if (address.length() > 0) {
                        Base.refData.put("address", address);
                    }else
                        Base.refData.put("customerType","ADSL1");
                }
	            LOGGER.info("Getting data from EVE DB");
	            Base.sessionvar.put("Address", Base.refData.getString("address"));
	        }
	       }else if(Base.refData.containsKey("address")) {
	           Base.sessionvar.put("Address", Base.refData.get("address"));
	           Base.sessionvar.put("INPLACE_TYPE", Base.refData.get("INPLACE_TYPE"));
	           LOGGER.info("Address element exists in scenario test data");
	       } else
	           LOGGER.info("Product builder scenario may fail no address element exists in ref data");
   }

   public static void loadrefDataWithoutAdddress(String scenario, String table) throws Exception {
       Base.refData= MongoDriver.getData(scenario,table);
   }

   public static Document getData(String scenario, String table) throws Exception{
       MongoDriver.ConnectToMongo("local");
       collection = database.getCollection(table);
       searchQuery.put("scenario",scenario);
       doc = collection.find(searchQuery);
       try {
           for (Document d : doc) {
               docs = d;
           }
       }
       catch (Exception e)
       {
         LOGGER.error("Either Mongo Database is not connected or Scenario ID not found in Mongo");
       }
       if (docs==null)
    	   LOGGER.info("mongo does NOT contains give scneario Id "+scenario+". Please connect to VPN");
       return docs;
   }

   public static FindIterable<Document> getAlldocs(String table){
       MongoDriver.ConnectToMongo("local");
       collection = database.getCollection(table);
       doc = collection.find(searchQuery);
       return doc;
   }

    public static Document getTestDataFromMongo(String queryId, String table, String matchColumn) throws Exception{
        MongoDriver.ConnectToMongo("local");
        collection = database.getCollection(table);
        searchQuery.put(matchColumn,queryId);
        doc = collection.find(searchQuery);
        try {
            for (Document d : doc) {
                docs = d;
            }
        }
        catch (Exception e)
        {
            LOGGER.error("Either Mongo Database is not connected or Scenario ID not found in Mongo");
        }
        if (docs==null)
            LOGGER.info("mongo does NOT contains give scneario Id "+queryId+". Please connect to VPN");
        return docs;
    }

   public static void writeNewDocument (Document inputDoc, String table){
        Base.sessionvar.put("createdTime",new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(new Date()));
        collection = database.getCollection(table);
        collection.insertOne(inputDoc);
    }

    public static void writeDump(Document oDump){
        collection = database.getCollection("PendingOrderPreProd");
        collection.insertOne(oDump);
    }
    
    
    public static void updateDocument(String table,String searchKey, Document sessionvar){

        BasicDBObject query = new BasicDBObject().append(searchKey, sessionvar.get(searchKey));
        sessionvar.put("updatedTime",new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(new Date()));
        BasicDBObject tempSet = new BasicDBObject().append("$set", sessionvar);
       try
       {
           collection = database.getCollection(table);
           collection.updateOne(query, tempSet);
           LOGGER.info("updated document" + query.getString(searchKey) + "for element" + sessionvar.toString() );
           
       }
       catch (Exception e)
       {
           LOGGER.error("unable to update the document \n"+e );
       }
    }

    public static void upsertDocument(String table,String searchKey, Document sessionvar) throws Exception {
        BasicDBObject query = new BasicDBObject().append(searchKey, sessionvar.get(searchKey));
        sessionvar.put("updatedTime",new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(new Date()));
        LOGGER.info("search using query"+query+" to table "+table);
        try{
            collection = database.getCollection(table);
            collection.replaceOne(query,sessionvar,updateOptions.upsert(true));
            LOGGER.info("upsert successful");
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception("mongo upsert error");
        }

    }

    public static void upsertDocumentUsingQuery(String table,Document query, Document sessionvar) throws Exception {
        sessionvar.put("updatedTime",new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(new Date()));
        LOGGER.info("search using query"+query+" to table "+table);
        try{
            collection = database.getCollection(table);
            collection.replaceOne(query,sessionvar,updateOptions.upsert(true));
            LOGGER.info("upsert successful");
        }catch (Exception e){
            throw new Exception("mongo upsert error");
        }

    }

    public static void disconnectMongo(){
    	
    	try{
    		mongoClient.close();
    		LOGGER.info("Mongo connection closed successfully");
    	}catch(Exception e){
    	    LOGGER.warn( "No mongo connection found." );
    	}
    }
    
    public static void deleteDocument(String table,String key, Document sessionvar){

        BasicDBObject query = new BasicDBObject().append(key, sessionvar.get(key));
       try
       {
           collection = database.getCollection(table);
           collection.deleteOne(query);
           
       }
       catch (Exception e)
       {
           LOGGER.error("unable to delete the document \n"+e );
       }
    }

    public static void deleteAllDocumentsFromMongoTable(String table){
        try
        {
            collection = database.getCollection(table);
            collection.deleteMany(new Document());

        }
        catch (Exception e)
        {
            LOGGER.error("unable to delete the document \n"+e );
        }
    }


    public static Document searchRandomDataFromMongo(String table, Document searchQuery) throws Exception {
        MongoDriver.ConnectToMongo("local");
        collection = database.getCollection(table);
        LOGGER.info("Getting Mongo Data from table "+ table + " with Filter Query  "+searchQuery);
        AggregateIterable<Document> output =  collection.aggregate(Arrays.asList(
                new Document("$match",searchQuery),
                new Document("$sample", new Document("size",1))
                ));


        for (Document d: output) {
            return d;
        }
        throw new Exception("No users found for the filter combination "+ searchQuery);
        }

    public static Document searchRandomDataFromMongo(String queryID) throws Exception {
        MongoDriver.ConnectToMongo("local");
        Document filter = MongoDriver.getTestDataFromMongo(queryID,"queries", "queryID");
        System.out.println(filter);
        try {
            collection = database.getCollection(filter.getString("collection"));

            filter.remove("_id");
            filter.remove("collection");
            filter.remove("queryID");

            if(!filter.containsKey("environment)"))
                filter.put("environment", Configuration.getEnvironmentProperties().getProperty("env.to.test"));
            LOGGER.info("Getting Mongo Data with Filter Query  " + filter);
            AggregateIterable<Document> output = collection.aggregate(Arrays.asList(
                    new Document("$match", filter),
                    new Document("$sample", new Document("size", 1))
            ));

            for (Document d : output) {
                return d;
            }
            throw new Exception("Error finding data for query in MONGO"+ queryID);
        }catch (Exception e){
            throw new Exception(e.getMessage());

        }

    }

    public static void moveCollection(String table, String environment) throws ScriptException {
        MongoDriver.connectMongo();
        collection = database.getCollection(table);
        Document search =new Document();
        Document query = new Document();
        Document delete = new Document();

        search.append("environment",environment);
        if(table.equalsIgnoreCase("resultSet")) {
            search.append("fixedUser", "yes");
            search.append("mobileUser", "no");

            FindIterable<Document> col = collection.find(search).limit(20);
            try {
                for (Document d : col) {

                    delete.put("customerEmail", d.getString("customerEmail"));
                    database.getCollection(table).deleteOne(delete);
                    d.remove("_id");
                    query.append("customerEmail", d.getString("customerEmail"));
                    database.getCollection("users").replaceOne(query, d, updateOptions.upsert(true));
                    LOGGER.info("Moved data to users table" + d);
                }
            }catch (Exception e){
                LOGGER.info("error updating fixed user data");
            }

            search.put("mobileUser","yes");
            query.clear();
            delete.clear();
            FindIterable<Document> coll2 = collection.find(search).limit(20);

            try {
                for (Document d : coll2) {

                    delete.put("customerEmail", d.getString("customerEmail"));
                    database.getCollection(table).deleteOne(delete);
                    d.remove("_id");
                    query.append("customerEmail", d.getString("customerEmail"));
                    database.getCollection("users").replaceOne(query, d, updateOptions.upsert(true));
                    LOGGER.info("Moved data to users table" + d);

                }
            }catch (Exception e){
                LOGGER.info("error updating mixed user data");
            }

        }

        search.clear();
        query.clear();
        delete.clear();
        if(table.equalsIgnoreCase("simData")) {
            search.append("octane_status", "USED");
            search.append("fixedUser", "no");

            FindIterable<Document> coll3 = collection.find(search).limit(20);

            try {
                for (Document d : coll3) {

                    delete.put("customerEmail", d.getString("customerEmail"));
                    database.getCollection(table).deleteOne(delete);
                    d.remove("_id");
                    query.append("customerEmail", d.getString("customerEmail"));
                    database.getCollection("users").replaceOne(query, d, updateOptions.upsert(true));
                    LOGGER.info("Moved data to users table" + d);
                }
            }catch (Exception e){
                LOGGER.info("error updating mobile user data");
            }
        }

    }

    public static Document projectSearch(){
        String e;
        MongoDriver.ConnectToMongo("local");
        collection = database.getCollection("resultSet");
        FindIterable<Document> coll3 = collection.find().limit(20).projection(fields(include("utilBillID"),exclude("_id")));
        for (Document d: coll3) {
            e = d.getString("utilBillID")+",\n";
            System.out.println(e);
        }
        return null;
    }


    public static void exportMongoData(String queryID) throws Exception {
        Document filter = getTestDataFromMongo(queryID,"queries","queryID");
        List<String> columns = (List<String>) filter.get("columns");
        filter.remove("_id");
        filter.remove("queryID");
        filter.remove("columns");
        MongoDriver.ConnectToMongo("local");
        collection = database.getCollection("resultSet");
        FindIterable<Document> docs = collection.find(filter);
        String CSVFile = System.getProperty("user.dir")+"\\src\\test\\resources\\users1.csv";
        FileWriter fileWriter = new FileWriter(CSVFile);
        for (String s: columns)
            fileWriter.write(s+",");
        fileWriter.write("\n");
        for (Document d: docs
             ) {
                fileWriter.write(buildFormat(columns,d) + "\n");
        }
        fileWriter.close();

    }

    public static String buildFormat(List<String> col, Document d) {
        String row = "";
        try {
            for (String c : col
                    ) {

                if (d.containsKey(c)) {
                    row = row + d.getString(c).replace(",","") + ",";
                } else
                    row = row + ",";

            }
        }catch (Exception e){
            return row;
        }
        return row;
    }



}