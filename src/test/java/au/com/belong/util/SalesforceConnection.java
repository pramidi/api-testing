package au.com.belong.util;

import au.com.belong.Configuration;
import com.sforce.soap.enterprise.Connector;
import com.sforce.soap.enterprise.EnterpriseConnection;
import com.sforce.soap.enterprise.LoginResult;

import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SalesforceConnection {

	private static final Logger LOGGER = LoggerFactory.getLogger(SalesforceConnection.class);

	// static final String USERNAME = "yixjiang@deloitte.com.au.com.belong.dev1";
	// static final String PASSWORD = "Belong123!Em3LYWOrTU2f8yK8CE2OqL74D";

	 //static final String USERNAME = "kenneth.ma@team.telstra.com.belong.uat";
     //static final String PASSWORD = "abcd1234Gx0IbB6ojCI1itVMpXE26Yv4R";
	
//	static final String USERNAME;
//	static final String PASSWORD;

	public static EnterpriseConnection connection;
	public EnterpriseConnection manualEnterpriseConnection;



	private String organizationId;
	private String portalId;
	private static LoginResult manualLoginResult;

	public static ConnectorConfig config = new ConnectorConfig();

	public void manualSFConnection(String username, String password) throws ConnectionException {


		config.setManualLogin(true);
		config.setAuthEndpoint("https://test.salesforce.com/services/Soap/c/40.0/00Dp0000000CvseEAC");
		config.setServiceEndpoint("https://test.salesforce.com/services/Soap/c/40.0/00Dp0000000CvseEAC");
		config.setManualLogin(true);
		manualEnterpriseConnection = new EnterpriseConnection(config);
		manualEnterpriseConnection.setLoginScopeHeader(this.organizationId, this.portalId);
		try{
			LOGGER.info("Establishing Saleforce connection manually...");
			this.setManualLoginResult(manualEnterpriseConnection.login(username,password));
			LOGGER.info("Manual salesforce successful with username " + username + " password "+ password );
		}catch (Exception e){
			LOGGER.error("manual login to salesforce is failed");
		}

	}

	public static void connectSalesforceWS(){

		config.setManualLogin(false);
		config.setAuthEndpoint("https://test.salesforce.com/services/Soap/c/40.0/00Dp0000000CvseEAC");
		config.setServiceEndpoint("https://test.salesforce.com/services/Soap/c/40.0/00Dp0000000CvseEAC");

//		config.setUsername(com.belong.Configuration.getEnvironmentProperties().getProperty("salesforce.username"));
//		config.setPassword(com.belong.Configuration.getEnvironmentProperties().getProperty("salesforce.password"));
		config.setUsername(Configuration.getEnvironmentProperties().getProperty("salesforce.admin.username"));
		config.setPassword(Configuration.getEnvironmentProperties().getProperty("salesforce.admin.password"));

		System.out.println("Auth EndPoint:" + config.getAuthEndpoint());
		System.out.println("Service EndPoint:" + config.getServiceEndpoint());
		System.out.println("Username: " + config.getUsername());
		System.out.println("SessionId: " + config.getSessionId());


		LOGGER.info("Establishing Saleforce connection...");

		try {
			connection  = Connector.newConnection(config);


			// display some current settings

			System.out.println("Auth EndPoint:" + config.getAuthEndpoint());
			System.out.println("Service EndPoint:" + config.getServiceEndpoint());
			System.out.println("Username: " + config.getUsername());
			System.out.println("password: " + config.getPassword());
			System.out.println("SessionId: " + config.getSessionId());

//			  com.sforce.soap.enterprise.QueryResult queryResults = connection.query( "SELECT Id FROM Account where Octane_Customer_Number__c='" + JoinGlobalConfigHolder.getCustomerNumber() + "'");
//			  if(queryResults.getSize() > 0) {
//			  System.out.println("Account Id "+queryResults.getRecords()[0]); ; }
//			  else {
//			  System.out.println("Can't find Salesforce account using UtiliBillId "); }

		} catch (ConnectionException e1) {
			LOGGER.error("Error connecting salesforce");
			e1.printStackTrace();
		}

	}

	public String getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getPortalId() {
		return this.portalId;
	}

	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	public EnterpriseConnection EstablishSalesforceConnection() {
		if(connection==null) {
			connectSalesforceWS();
		}
		return connection;
	}

	public static void disconnectSF() throws ConnectionException {
		try {
			connection.logout();
			LOGGER.info("logged out salesforce successfully");
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setManualLoginResult(LoginResult manualLoginResult) {
		this.manualLoginResult = manualLoginResult;
	}

	public LoginResult getManualLoginResult(){
		return this.manualLoginResult;
	}

}
