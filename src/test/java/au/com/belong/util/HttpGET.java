package au.com.belong.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.bson.Document;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

public class HttpGET {

    public static Logger LOGGER = LoggerFactory.getLogger(HttpGet.class);

    public static JSONObject getRequest(String url, HashMap<String, String> params) throws Exception {
        JSONObject obj = null;
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setPath(url);
        for (Map.Entry<String,String> e : params.entrySet()){
            uriBuilder.addParameter(e.getKey(),e.getValue());
        }
        URI uri = uriBuilder.build();
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(uri);
            request.addHeader("content-type", "application/json");
            HttpResponse result = httpClient.execute(request);

            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            if(json.contains("503 Service Unavailable"))
                throw new Exception(json);
            try {

                JSONParser parser = new JSONParser();
                Object resultObject = parser.parse(json);

                if (resultObject instanceof JSONArray) {
                    JSONArray array = (JSONArray) resultObject;
                    for (Object object : array) {
                        obj = (JSONObject) object;
                    }
                } else if (resultObject instanceof JSONObject) {
                    obj = (JSONObject) resultObject;
                }
                System.out.println("Get requests is successful" + obj);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error while making GET");

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }

    public static JSONObject hitApi(String url, HashMap<String, String> params, HashMap<String, String> headers) throws Exception {
        JSONObject obj = null;
        HttpResponse result = null;
        URIBuilder uriBuilder = new URIBuilder();
        uriBuilder.setPath(url);
        LOGGER.info("hitting url "+url);
        URI uri = uriBuilder.build();
        HttpGet request = new HttpGet(uri);
        request.addHeader("content-type", "application/json");

        TrustManager[] dumyTrust = new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                    }

                    @Override
                    public X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }
                }
        };

        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null,dumyTrust,new SecureRandom());



        for (Map.Entry<String,String> e : params.entrySet()){
            uriBuilder.addParameter(e.getKey(),e.getValue());
        }

        for(Map.Entry<String,String> e: headers.entrySet()){
            if(e.getValue().length()>0)
                request.addHeader(e.getKey(),e.getValue());
        }

//        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

        SSLContextBuilder builder = new SSLContextBuilder();
        builder.loadTrustMaterial(null, new TrustStrategy() {
            @Override
            public boolean isTrusted(final X509Certificate[] x509Certificates, String s) throws CertificateException {
                return true;
            }
        });
        
        SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(builder.build());
            try (CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build()) {

            result = httpClient.execute(request);

            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            if(json.contains("503 Service Unavailable"))
                throw new Exception(json);
            else
                LOGGER.info(json);
            try {

                JSONParser parser = new JSONParser();
                Object resultObject = parser.parse(json);

                if (resultObject instanceof JSONArray) {
                    JSONArray array = (JSONArray) resultObject;
                    for (Object object : array) {
                        obj = (JSONObject) object;
                    }
                } else if (resultObject instanceof JSONObject) {
                    obj = (JSONObject) resultObject;
                }
                System.out.println("Get requests is successful" + obj);
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error while making GET");

            }
        } catch (IOException e) {
                LOGGER.info(result.toString());
            e.printStackTrace();
        }
        return obj;
    }


    private boolean recheckApi(Document refData, Map.Entry<String, Object> m, Document outcome) {

        Document d = (Document) refData.get(m.getKey());
        String s = outcome.get("response").toString();
        LOGGER.info("comparing texts at Recheck API "+ s + " with " + d.getString("messages"));
        if(s.toLowerCase().contains(d.getString("messages").toLowerCase()))
            return true;
        else
            return false;
    }


}