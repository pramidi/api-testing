package au.com.belong.util;

import au.com.belong.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;


public class DatabaseConnection

{
	private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConnection.class);

	private String databaseUrl;
	private String databaseUsername;
	private String databasePassword;
	public static Connection conn;
	public Connection c;

	public DatabaseConnection()
	{
		if (Configuration.getEnvironmentProperties().getProperty("ssh.required").equalsIgnoreCase("true")) {
			databaseUrl = Configuration.getEnvironmentProperties().getProperty("ssh.jdbc.databaseurl");
		} else {
			databaseUrl = System.getProperty("jdbc.databaseurl");
		}
		databaseUsername = System.getProperty("jdbc.username");
		databasePassword = System.getProperty("jdbc.password");
	}

	public void setConnection(Connection c){
		conn = c;
	}


	public Connection connectEveDB(String databaseUrl, String databaseUsername, String databasePassword) throws SQLException {
		try {
			return DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
		}catch (Exception e){
			LOGGER.info("Connection to EVE DB failed");
		}
		return null;
	}

	public void connectEveDB() throws SQLException {
		try {
			conn = (Connection) DriverManager.getConnection(databaseUrl, databaseUsername, databasePassword);
		}catch (Exception e){
			LOGGER.info("Connection to EVE DB failed");
		}
	}

	public static void disconnectEveDB() throws SQLException {
		//try {
		if(conn!=null) {
			conn.close();
		}

		//}catch(Exception e) {
			//LOGGER.info("No EVE DB connection to close.");
		//}
	}

	//static connection for query
	public ResultSet processSQLStatement(String sql)
	{
		ResultSet rs = null;
		try
		{

			PreparedStatement stmt = conn.prepareStatement(sql);

			rs = stmt.executeQuery();
			// STEP 5: Extract data from result set
			return rs;
		}
		catch (SQLException e)
		{
			LOGGER.error("SQLException", e);
		}
		return rs;
	}

	//dynamic connections to make a query
	public ResultSet processSQLStatement(String sql, Connection c)
	{
		ResultSet rs = null;
		try
		{

			PreparedStatement stmt = c.prepareStatement(sql);

			rs = stmt.executeQuery();
			// STEP 5: Extract data from result set
			return rs;
		}
		catch (SQLException e)
		{
			LOGGER.error("SQLException", e);
		}
		return rs;
	}

	public ResultSet processSQLStatement(String sql, String mobile)
	{
		ResultSet rs = null;
		try
		{
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setString(1, mobile);

			rs = stmt.executeQuery();
			// STEP 5: Extract data from result set
			return rs;
		}
		catch (SQLException e)
		{
			LOGGER.error("SQLException", e);
		}
		return rs;
	}

	public void executeUpdate(String sql)
	{
		try
		{
			PreparedStatement stmt = conn.prepareStatement(sql);

			stmt.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("SQLException", e);
		}
	}

	public void executeUpdateWithDate(String sql, String mobile)
	{
		try
		{
			PreparedStatement stmt = conn.prepareStatement(sql);
			stmt.setDate(1, new Date(0));
			stmt.setString(2, mobile);

			stmt.executeUpdate();
		}
		catch (SQLException e)
		{
			LOGGER.error("SQLException", e);
		}
	}
}
