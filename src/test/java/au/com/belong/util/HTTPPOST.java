package au.com.belong.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;


public  class HTTPPOST 

{
	private static final Logger LOGGER = LoggerFactory.getLogger(HTTPPOST.class);

	public JSONObject service(String url,String requestBody) {

		String ResponseCode = "";
		try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {

			HttpPost request = new HttpPost(url);
			StringEntity params = new StringEntity(requestBody);
			System.out.println(requestBody);
			request.addHeader("content-type", "application/json");
			request.setEntity(params);
			LOGGER.info("Making API call - "+url);
			LOGGER.info("Request Body: " +request);
			HttpResponse result = httpClient.execute(request);
			String json = EntityUtils.toString(result.getEntity(), "UTF-8");
			LOGGER.info(json);

			JSONParser parser = new JSONParser();
			return  (JSONObject)parser.parse(json);

		} catch (IOException | ParseException ex) {
			LOGGER.info("POST Request Failed"+ ex.getMessage());
			ex.printStackTrace();


		}
		return null;
	}



	


	public String createKeyValuePair(String key, String value, String keyValuePair) {
		if(keyValuePair.isEmpty()) {
			keyValuePair = key+":"+value;
		}
		else {
			keyValuePair = keyValuePair +"," + key+":"+value;
		}
		return keyValuePair;
	}
}


