package au.com.belong.api;

import au.com.belong.api.service.ServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Mulesoft {


    Logger LOGGER = LoggerFactory.getLogger(Mulesoft.class);
    public void callApi(String api) throws Exception {

        ServiceInterface serviceInterface = (ServiceInterface) Class.forName("au.com.belong.api.service.impl."+api).newInstance();
        serviceInterface.hitApi(api);
    }

    public void compareApiResponse(String muleApi, String prodApi) throws NoSuchFieldException, IOException {
        HashMap mule = new HashMap();
        HashMap prod = new HashMap();
        try {
            mule = getKeyValueFromObject(ApiResponseFactory.class.getField(muleApi).get(new ApiResponseFactory()));
            prod = getKeyValueFromObject(ApiResponseFactory.class.getField(prodApi).get(new ApiResponseFactory()));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }


//        InputStream inputStream = new FileInputStream("keyMapper.properties");
        Properties properties = new Properties();
        properties.load(Mulesoft.class.getClassLoader().getResourceAsStream("properties/keyMapper.properties"));
        mapAndCompareResponses(mule,prod, properties);

    }

    private void mapAndCompareResponses(HashMap<String, String> mule, HashMap<String, String> prod, Properties props) {

        for (Map.Entry<String, String> e: mule.entrySet()
             ) {

            try {
                if(!compare(e.getValue(),prod.get(props.getProperty(e.getKey()))))
                    LOGGER.info(e.getKey() +":" +e.getValue()+" is not matching with "+ props.getProperty(e.getKey())+":" +prod.get(e.getKey()));
                if(!((e.getValue().equals("null"))|| prod.get(props.getProperty(e.getKey())).equals("null")))
                    throw new Exception(e.getKey() + " is not matching with prod");
            }catch (Exception err){
                LOGGER.info(err.getMessage());
            }

        }
    }


    private boolean compare(String source, String target){
        return (source.equalsIgnoreCase(target));
    }

    public HashMap getKeyValueFromObject(Object c){
        HashMap<String, String> m = new HashMap<>();

        for (Field f: c.getClass().getFields()
             ) {
            try {
                m.put(f.getName(), String.valueOf(f.get(c)));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return m;
    }
}
