package au.com.belong.api.service.impl;

import au.com.belong.Configuration;
import au.com.belong.api.ApiResponseFactory;
import au.com.belong.api.responseBodyAttributes.UtilityTokenResponse;
import au.com.belong.api.service.ServiceInterface;
import au.com.belong.util.HttpGET;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sforce.ws.ConnectionException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import au.com.belong.util.SalesforceConnection;

import java.io.IOException;
import java.util.HashMap;

public class UtilityToken extends ApiResponseFactory implements ServiceInterface {

    private static Logger LOGGER = LoggerFactory.getLogger(UtilityToken.class);
    private SalesforceConnection salesforceConnection = new SalesforceConnection();

    public UtilityTokenResponse getUtilityApiResponse() {
        return UtilityToken;
    }

    public void setUtilityApiResponse(UtilityTokenResponse utilityApiRes) {
        UtilityToken = utilityApiRes;
        LOGGER.info(UtilityToken.toString());
    }

    public String getSalesforceSessionId(){
        if(salesforceConnection.getManualLoginResult()!=null){
            return salesforceConnection.getManualLoginResult().getSessionId();
        }else {
            salesforceConnection.setOrganizationId(Configuration.getEnvironmentProperties().getProperty("salesforce.organizationId"));
            salesforceConnection.setPortalId(Configuration.getEnvironmentProperties().getProperty("salesforce.portalId"));
            try {
                salesforceConnection.manualSFConnection("test-acmabfm-170@mailinator.com", "test1234");
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
            System.out.println(salesforceConnection.getManualLoginResult().getSessionId());
            return salesforceConnection.getManualLoginResult().getSessionId();
        }
    }

    @Override
    public void hitApi(String api) throws ConnectionException {

        HashMap<String,String> m = new HashMap<>();
        m.put("customerId","test-acmabfm-170@mailinator.com");
        m.put("sessionId", getSalesforceSessionId());
        m.put("X-Consumer-ID","sdfd");

        JSONObject response = null;

        try {
            response = HttpGET.hitApi(Configuration.getEnvironmentProperties().getProperty("mule.test.api.url")+
                            Configuration.getEnvironmentProperties().getProperty(api),
                    new HashMap<>(),m);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            this.setUtilityApiResponse(new ObjectMapper().readValue(response.toJSONString(), UtilityTokenResponse.class));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void getResponse() {

    }

    @Override
    public void hitprodApi(String api) {

    }

    @Override
    public void validateResponse() {

    }
}
