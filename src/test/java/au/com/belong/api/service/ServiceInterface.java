package au.com.belong.api.service;

import com.sforce.ws.ConnectionException;

public interface ServiceInterface {

    void hitApi(String api) throws ConnectionException;
    void getResponse();
    void hitprodApi(String api);
    void validateResponse();
}