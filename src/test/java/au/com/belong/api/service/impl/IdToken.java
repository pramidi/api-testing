package au.com.belong.api.service.impl;

import au.com.belong.Configuration;
import au.com.belong.api.ApiResponseFactory;
import au.com.belong.api.responseBodyAttributes.IdTokenResponse;
import au.com.belong.api.service.ServiceInterface;
import au.com.belong.util.HTTPPOST;
import au.com.belong.util.SalesforceConnection;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sforce.ws.ConnectionException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class IdToken extends ApiResponseFactory implements ServiceInterface {

    private SalesforceConnection salesforceConnection = new SalesforceConnection();
    private static Logger LOGGER = LoggerFactory.getLogger(IdToken.class);

    public IdTokenResponse getIdTokenResponse() {
        return IdToken;
    }

    public void setIdTokenResponse(IdTokenResponse idTokenResp) {
        IdToken = idTokenResp;
        LOGGER.info(IdToken.toString());
    }

    public String getSalesforceSessionId(){
        if(salesforceConnection.getManualLoginResult().getSessionId()!=null){
            return salesforceConnection.getManualLoginResult().getSessionId();
        }else {
            salesforceConnection.setOrganizationId(Configuration.getEnvironmentProperties().getProperty("salesforce.organizationId"));
            salesforceConnection.setPortalId(Configuration.getEnvironmentProperties().getProperty("salesforce.portalId"));
            try {
                salesforceConnection.manualSFConnection("test-acmabfm-170@mailinator.com", "test1234");
            } catch (ConnectionException e) {
                e.printStackTrace();
            }
            System.out.println(salesforceConnection.getManualLoginResult().getSessionId());
            return salesforceConnection.getManualLoginResult().getSessionId();
        }
    }

    @Override
    public void hitApi(String api) throws ConnectionException {

        JSONObject d = new JSONObject();
        HTTPPOST httppost = new HTTPPOST();
        JSONObject request = new JSONObject();
        request.put("email","test-acmabfm-170@mailinator.com");
        request.put("password",getSalesforceSessionId());
        JSONObject res = httppost.service(Configuration.getEnvironmentProperties().getProperty("api.url")+
                Configuration.getEnvironmentProperties().getProperty(api),request.toString());
        JSONObject response = (JSONObject) res.get("idToken");
        LOGGER.info(String.valueOf(response));
        try {
            this.setIdTokenResponse(new ObjectMapper().readValue(response.toJSONString(), IdTokenResponse.class));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void getResponse() {

    }

    @Override
    public void hitprodApi(String api) {



    }

    @Override
    public void validateResponse() {

    }
}
