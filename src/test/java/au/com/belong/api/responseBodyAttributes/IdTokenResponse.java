package au.com.belong.api.responseBodyAttributes;

public class IdTokenResponse {
    public String Email;
    public String Mobile_Octane_ID;
    public String Broadband_Octane_ID;
    public String First_Name;
    public String Last_Name;
    public String Role_Name;
    public String Expiration_Time;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String success;

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String errorType;

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile_Octane_ID() {
        return Mobile_Octane_ID;
    }

    public void setMobile_Octane_ID(String mobile_Octane_ID) {
        Mobile_Octane_ID = mobile_Octane_ID;
    }

    public String getBroadband_Octane_ID() {
        return Broadband_Octane_ID;
    }

    public void setBroadband_Octane_ID(String broadband_Octane_ID) {
        Broadband_Octane_ID = broadband_Octane_ID;
    }

    public String getFirst_Name() {
        return First_Name;
    }

    public void setFirst_Name(String first_Name) {
        First_Name = first_Name;
    }

    public String getLast_Name() {
        return Last_Name;
    }

    public void setLast_Name(String last_Name) {
        Last_Name = last_Name;
    }

    public String getRole_Name() {
        return Role_Name;
    }

    public void setRole_Name(String role_Name) {
        Role_Name = role_Name;
    }

    public String getExpiration_Time() {
        return Expiration_Time;
    }

    public void setExpiration_Time(String expiration_Time) {
        Expiration_Time = expiration_Time;
    }

}
