package au.com.belong.api.responseBodyAttributes;

public class UtilityTokenResponse {
    public String customerId;
    public String emailId;
    public String mobileOctaneId;
    public String broadbandOctaneId;
    public String firstName;
    public String lastName;
    public String userRoleForValidation;
    public String customerType;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobileOctaneId() {
        return mobileOctaneId;
    }

    public void setMobileOctaneId(String mobileOctaneId) {
        this.mobileOctaneId = mobileOctaneId;
    }

    public String getBroadbandOctaneId() {
        return broadbandOctaneId;
    }

    public void setBroadbandOctaneId(String broadbandOctaneId) {
        this.broadbandOctaneId = broadbandOctaneId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserRoleForValidation() {
        return userRoleForValidation;
    }

    public void setUserRoleForValidation(String userRoleForValidation) {
        this.userRoleForValidation = userRoleForValidation;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }


}
