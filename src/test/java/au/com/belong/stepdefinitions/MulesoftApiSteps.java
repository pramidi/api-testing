package au.com.belong.stepdefinitions;

import au.com.belong.api.Mulesoft;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class MulesoftApiSteps {

    Mulesoft mulesoft = new Mulesoft();

    @Then("^I validate against the <database>$")
    public void iValidateAgainstTheDatabase() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }

    @Given("^I hit the \"([^\"]*)\"$")
    public void iHitThe(String api) throws Throwable {
        mulesoft.callApi(api);
    }

    @Then("^I validate against the \"([^\"]*)\"$")
    public void iValidateAgainstThe(String arg0) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    }

    @Then("^I caompare response with \"([^\"]*)\" and \"([^\"]*)\" responses$")
    public void iCaompareResponseWithAnd(String muleApi, String prodApi) throws Throwable {
        mulesoft.compareApiResponse(muleApi,prodApi);
    }
}
