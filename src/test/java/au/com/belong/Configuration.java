package au.com.belong;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Configuration
{
	private static String OS = System.getProperty("os.name").toLowerCase();
	private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);
	
	public void setUpTestingEnvironmentVariables()
	{
		File chrome = null;
		File firefox = null;
		File ie = null;

		if (isWindows())
		{
			chrome = new File(getEnvironmentProperties().getProperty("chrome.driver.path"));
			System.setProperty("webdriver.chrome.driver", chrome.getAbsolutePath());
			firefox = new File(getEnvironmentProperties().getProperty("firefox.driver.path"));
			System.setProperty("webdriver.firefox.bin", firefox.getAbsolutePath());			
			ie = new File(getEnvironmentProperties().getProperty("ie.driver.path"));
			System.setProperty("webdriver.ie.driver", ie.getAbsolutePath());
		}
		else if (isMac())
		{
			chrome = new File(getEnvironmentProperties().getProperty("chrome.driver.path.osx"));
			System.setProperty("webdriver.chrome.driver", chrome.getAbsolutePath());
			// on a Mac, we don't need to define Firefox's location.
		}
		else if (isUnix())
		{
			LOGGER.info("Using UNIX environment properties");
			chrome = new File(getEnvironmentProperties().getProperty("chrome.driver.path.unix"));
			System.setProperty("webdriver.chrome.driver", chrome.getAbsolutePath());
			firefox = new File(getEnvironmentProperties().getProperty("firefox.driver.path.unix"));
			System.setProperty("webdriver.firefox.bin", firefox.getAbsolutePath());
			String iePath = System.getProperty("user.dir")+"src\\config\\iedriver";
			System.setProperty("webdriver.ie.driver", iePath);
		}
		else
		{
			LOGGER.error("Unknown operating system!!");
		}
		
		System.setProperty("domain.to.test", getEnvironmentProperties().getProperty("domain.to.test"));
		System.setProperty("jdbc.databaseurl", getEnvironmentProperties().getProperty("jdbc.databaseurl"));
		System.setProperty("jdbc.username", getEnvironmentProperties().getProperty("jdbc.username"));
		System.setProperty("jdbc.password", getEnvironmentProperties().getProperty("jdbc.password"));
		System.setProperty("address.jdbc.databaseurl", getEnvironmentProperties().getProperty("address.jdbc.databaseurl"));
		System.setProperty("address.jdbc.username", getEnvironmentProperties().getProperty("address.jdbc.username"));
		System.setProperty("address.jdbc.password", getEnvironmentProperties().getProperty("address.jdbc.password"));
//		System.setProperty("javax.net.ssl.trustStore", getEnvironmentProperties().getProperty("javax.net.ssl.trustStore"));
//		System.setProperty("avax.net.ssl.trustStorePassword", getEnvironmentProperties().getProperty("avax.net.ssl.trustStorePassword"));
	}
	
	
	public List<WebDriver> getBrowswersToExecuteTestsIn()
	{
		List<WebDriver> driverList = new ArrayList<WebDriver>();
		driverList.add(new FirefoxDriver());
		// driverList.add(new InternetExplorerDriver());
		driverList.add(new ChromeDriver());
		return driverList;
	}
	
	public static Properties getEnvironmentProperties()
	{
		Properties prop = new Properties();
		InputStream environment_properties = null;
		InputStream api_properties = null;
		InputStream local_environment_properties = null;
		InputStream data_environment = null;
		String environment = null;
		
		environment_properties = Configuration.class.getClassLoader().getResourceAsStream("properties/environment.properties");
		api_properties = Configuration.class.getClassLoader().getResourceAsStream("properties/api-endpoints.properties");
		local_environment_properties = Configuration.class.getClassLoader().getResourceAsStream("properties/local-environment.properties");
		
		environment = System.getenv("env");
		if (environment!=null){
			Base.sessionvar.put("environment", environment);
			data_environment = Configuration.class.getClassLoader().getResourceAsStream("properties/"+System.getenv("env").toLowerCase() +"-data.properties");
		}
		else
		{
			if(System.getProperty("env")!=null)
			{
//				System.out.println("Env set from maven properties" + System.getProperty("env").toLowerCase());
				data_environment = Configuration.class.getClassLoader().getResourceAsStream("properties/"+System.getProperty("env").toLowerCase() + "-data.properties");
				Base.sessionvar.put("environment", System.getProperty("env").toLowerCase());
			}
			else
				{
				data_environment = Configuration.class.getClassLoader().getResourceAsStream("properties/"+getTestRunEnvironment().getProperty("env.to.test").toLowerCase() + "-data.properties");
				Base.sessionvar.put("environment", getTestRunEnvironment().getProperty("env.to.test").toLowerCase());
			}
		}


		
		/*
		 if (environment!=null){
			//data_environment = com.belong.Configuration.class.getClassLoader().getResourceAsStream(environment + "-data.properties");
			data_environment = com.belong.Configuration.class.getClassLoader().getResourceAsStream(System.getProperty("env").toLowerCase() +"-data.properties");
			
		}
		else{
			data_environment = com.belong.Configuration.class.getClassLoader().getResourceAsStream(getTestRunEnvironment().getProperty("env.to.test").toLowerCase() +"-data.properties");
			
		}*/
		
		SequenceInputStream properties = new SequenceInputStream(environment_properties, local_environment_properties);
		SequenceInputStream propertiesWithApi = new SequenceInputStream(properties, api_properties);
		SequenceInputStream input = new SequenceInputStream(propertiesWithApi, data_environment);
		
		try
		{
			prop.load(input);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return prop;
	}
	
	/*private static String getRuntimeValue(String value) {
		String pomProperty = System.getProperty(value);
		LOGGER.info("property in POM value passed through maven was" + pomProperty);
		
		if((value=="env")&&(pomProperty!=null)){
		final String[] envs = new String [] {"prod","stagining","preprod","test","test2"};
		if (Arrays.asList(envs).contains(pomProperty))
			return pomProperty;
		else
			return "invalid env";
		}
		
		return pomProperty;
	}*/



	private static Properties getTestRunEnvironment() {
		
		Properties testenvProp = new Properties();
		InputStream env_prop = null;
		
		env_prop = Configuration.class.getClassLoader().getResourceAsStream("properties/environment.properties");
		BufferedInputStream properties = new  BufferedInputStream(env_prop);
		
		try
		{
			testenvProp.load(properties);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return testenvProp;
	}

	public static boolean isWindows()
	{
		return (OS.indexOf("win") >= 0);
	}
	
	public static boolean isMac()
	{
		return (OS.indexOf("mac") >= 0);
	}
	
	public static boolean isUnix()
	{
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0);
	}
}
